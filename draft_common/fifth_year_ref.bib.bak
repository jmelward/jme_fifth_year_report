% This file was created with JabRef 2.7b.
% Encoding: UTF-8

@ARTICLE{core_shell_jme,
  author = {Elward, Jennifer M. and Chakraborty, A},
  title = {Investigation of the effect of core shell heterostructure on exciton
	dissociation in CdSe/ZnS quantum dots},
  journal = {to be submitted to ACS Nano},
  year = {2014},
  owner = {jen},
  timestamp = {2014.03.27}
}

@ARTICLE{jme_pseudo,
  author = {Elward, Jennifer M. and Chakraborty, Arindam},
  title = {Investigation of size effects on CdSe quantum dots using explicitly
	correlated fully atomistic pseudopotential metho},
  journal = {to be submitted to Journal of Chemical Theory and Computation},
  year = {2014},
  owner = {jen},
  timestamp = {2014.03.27}
}

@INPROCEEDINGS{aps_1,
  author = {Elward, Jennifer M. and Chakraborty, Arindam},
  title = {Investigation of the effect of core shell interface on exciton binding
	energy and electron-hole recombination probability in CdSe/ZnS quantum
	dots},
  booktitle = {Session R25: Focus Session: Computational Studies of Heterostructures
	American Physical Society March Meeting, Baltimore, MD},
  year = {2013},
  owner = {jen},
  timestamp = {2014.03.27}
}

@ARTICLE{doi:10.1021/ct400485s,
  author = {Elward, Jennifer M. and Chakraborty, Arindam},
  title = {Effect of Dot Size on Exciton Binding Energy and Electron-Hole Recombination
	Probability in CdSe Quantum Dots},
  journal = {Journal of Chemical Theory and Computation},
  year = {2013},
  volume = {9},
  pages = {4351-4359},
  number = {10},
  doi = {10.1021/ct400485s},
  owner = {jen},
  timestamp = {2014.03.27}
}

@ARTICLE{pra_paper,
  author = {Elward, Jennifer M. and Hoja, Johannes and Chakraborty, Arindam},
  title = {Variational solution of the congruently transformed Hamiltonian for
	many-electron systems using a full-configuration-interaction calculation},
  journal = {Phys. Rev. A},
  year = {2012},
  volume = {86},
  pages = {062504},
  month = {Dec},
  doi = {10.1103/PhysRevA.86.062504},
  issue = {6},
  numpages = {8},
  owner = {jen},
  publisher = {American Physical Society},
  timestamp = {2014.04.01},
  url = {http://link.aps.org/doi/10.1103/PhysRevA.86.062504}
}

@ARTICLE{lucif_cdse,
  author = {Elward, Jennifer M. and Irudayanathan, Flaviyan I. and Nangia, Shikha
	and Chakraborty, Arindam},
  title = {Optical signature of formation of protein corona in the firefly Luciferase-CdSe
	quantum dot complex},
  journal = {to be submitted to Journal of Physical Chemistry Letters},
  year = {2014},
  owner = {jen},
  timestamp = {2014.03.27}
}

@INPROCEEDINGS{aps_acc,
  author = {Elward, Jennifer M. and Kaplan, Benjamin S. and Chakraborty, Arindam},
  title = {Construction of adiabatic connection curve for electron-hole system
	using multicomponent Levy-Lieb Lagrangian},
  booktitle = {Session V1: Poster Session III American Physical Society March Meeting,
	Baltimore, MD},
  year = {2013},
  owner = {jen},
  timestamp = {2014.03.27}
}

@INPROCEEDINGS{aps_2014,
  author = {Elward, Jennifer M. and Scher, Jeremy A. and Chakraborty, Arindam},
  title = {Investigation of effect of particle size and heterojunction on electron-hole
	interaction in CdSe and CdSe/ZnS quantum dots},
  booktitle = {Session Q27: Focus Session: Electron-hole Interaction in Nanoparticles
	American Physical Society March Meeting, Denver, CO},
  year = {2014},
  owner = {jen},
  timestamp = {2014.03.27}
}

@ARTICLE{acc_dft,
  author = {Elward, Jennifer M. and Scher, Jeremy A. and Kaplan, Benjamin S.
	and Chakraborty, Arindam},
  title = {Development of electron-hole adiabatic connection curve using explicitly
	correlated Hartree-Fock method for application to electron-hole density
	functional theory},
  journal = {to be submitted to Physical Review A},
  year = {2014},
  owner = {jen},
  timestamp = {2014.03.27}
}

@ARTICLE{zunger,
  author = {Franceschetti, Alberto and Zunger, Alex},
  title = {Direct Pseudopotential Calculation of Exciton Coulomb and Exchange
	Energies in Semiconductor Quantum Dots},
  journal = {Phys. Rev. Lett.},
  year = {1997},
  volume = {78},
  pages = {915--918},
  month = {Feb},
  doi = {10.1103/PhysRevLett.78.915},
  issue = {5}
}

@ARTICLE{cite_rabani,
  author = {Rabani, Eran and Hetényi, Balázs and Berne, B. J. and Brus, L. E.},
  title = {Electronic properties of CdSe nanocrystals in the absence and presence
	of a dielectric medium},
  journal = {The Journal of Chemical Physics},
  year = {1999},
  volume = {110},
  pages = {5355-5369},
  number = {11},
  doi = {http://dx.doi.org/10.1063/1.478431},
  owner = {jen},
  timestamp = {2014.03.30}
}

@ARTICLE{iso_vol,
  author = {Scher, Jeremy A. and Elward, Jennifer M. and Chakraborty, Arindam},
  title = {Shape Matters: Isovolumetric Transformations of CdSe Nanocrystals},
  journal = {to be submitted to ACS Nano},
  year = {2014},
  owner = {jen},
  timestamp = {2014.03.27}
}

@comment{jabref-meta: selector_publisher:}

@comment{jabref-meta: selector_author:}

@comment{jabref-meta: selector_journal:}

@comment{jabref-meta: selector_keywords:}

